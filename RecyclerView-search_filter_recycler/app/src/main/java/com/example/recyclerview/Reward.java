package com.example.recyclerview;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import java.util.ArrayList;
import java.util.List;

public class Reward extends AppCompatActivity {

    RecyclerView recyclerView;
    RecyclerAdapter recyclerAdapter;

    List<String> moviesList;

    Button btn1,btn2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reward);

        moviesList = new ArrayList<>();
        moviesList.add("Advertising 1 Day");
        moviesList.add("Advertising 7 Day");
        moviesList.add("Advertising 30 Day");
        moviesList.add("Advertising 1 Month");
        moviesList.add("Advertising 2 Month");
        moviesList.add("Advertising 3 Month");
        moviesList.add("Fan");
        moviesList.add("microwave");
        moviesList.add("Refrigerator");
        moviesList.add("Television");
        moviesList.add("Phone");
        moviesList.add("Motorcycle");
        moviesList.add("Car");

        recyclerView = findViewById(R.id.recyclerView);
        recyclerAdapter = new RecyclerAdapter(moviesList);
//        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(recyclerAdapter);

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(this, DividerItemDecoration.VERTICAL);
        recyclerView.addItemDecoration(dividerItemDecoration);

        btn1 = findViewById(R.id.textView);
        btn2 = findViewById(R.id.textView2);


        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(),Used.class);
                startActivity(i);
            }
        });

        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(),NotUse.class);
                startActivity(i);
            }
        });



    }



}